sudo chown -R $(whoami) /usr/local/var/homebrew

chgrp -R admin /usr/local
chmod -R g+w /usr/local

chgrp -R admin /Library/Caches/Homebrew
chmod -R g+w /Library/Caches/Homebrew

chgrp -R admin /opt/homebrew-cask
chmod -R g+w /opt/homebrew-cask

---

brew link node
brew link --overwrite node

brew link yarn
brew link --overwrite yarn