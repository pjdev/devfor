#!/bin/bash

DEVFOR_ROOT=$HOME/.devfor

declare -a REMOVE_LIST=(
    "$DEVFOR_ROOT/.clt"
    "$DEVFOR_ROOT/app.sh"
    "$DEVFOR_ROOT/brew.sh"
    "$DEVFOR_ROOT/Brewfile"
    "$DEVFOR_ROOT/font.sh"
    "$DEVFOR_ROOT/git.sh"
    "$DEVFOR_ROOT/init.log"
    "$DEVFOR_ROOT/install-linux.sh"
    "$DEVFOR_ROOT/install-mac.sh"
    "$DEVFOR_ROOT/mac.sh"
    "$DEVFOR_ROOT/nodejs.sh"
    "$DEVFOR_ROOT/shell.sh"
    "$DEVFOR_ROOT/ssh.sh"
    "$DEVFOR_ROOT/utils.sh"
    "$HOME/.bin"
    "$HOME/.gitconfig"
    "$HOME/.vim_mru_files"
    "$HOME/.zshrc.pre-oh-my-zsh"
    "$HOME/.czcrc"
    "$HOME/.npmrc"
    "$HOME/.vimrc"
    "$HOME/.zshrc"
    "$HOME/.zsh_history"
    "$HOME/.zshrc.pre-oh-my-zsh"
    "$HOME/.ssh/id_rsa"
    "$HOME/.ssh/id_rsa.pub"
    "$HOME/.itermocil"
    "$HOME/.oh-my-zsh"
    "$HOME/.vim_runtime"
    "$HOME/.vscode"
    "$HOME/.zsh-syntax-highlighting"
    "$DEVFOR_ROOT/devenv"
    "$DEVFOR_ROOT/fonts"
)

# Remove all
for i in "${REMOVE_LIST[@]}"
do
    rm -fr "$i"
done